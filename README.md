# tiveg challenge

The process is as follows:
1. each (family,store) have its own binary model with delta weights
   1. input layer consist in 5 enveloped variables with memory window of 60d -> Tensor in dimension (n, 60, 6): 
      1. promotion
      2. oil
      3. holiday
      4. cluster
      5. payday
      6. sale (previous)
   2. each model contains 4 layers:
      1. 20 LSTM
      2. 10 LSTM
      3. 4 NN
      4. 1 NN -> Sale prediction
2. running each model will generate an individual file under ``results/`` directory
3. running again, the model will use previous generated model acting as a delta update accelerating convergence and learn from previous execution


## Description
For further information, visit
https://www.kaggle.com/competitions/store-sales-time-series-forecasting/data


## Installation
1. If you are on linux, modify the requirements.txt in order to use ``tensorflow`` instead of ``tensorflow-macos`` which is for mac os
2. Execute installation script
   ```bash
    source install.sh
   ```

In the future to activate your environment, you can execute
``source install.sh`` or the classic 
```bash
  source venv/bin/activate
  ```

In some circumstances you will need to update your ``PYTHONPATH`` environment variable
if you need it type

```bash
export PYTHONPATH=$PWD:$PYTHONPATH
``` 

under the main project directory


## Usage
N execution with Delta updates
```bash
python app/lstm_model.py --debug --family=XYZ --store=666
python app/lstm_model.py --debug --family=XYZ --store=666
python app/lstm_model.py --debug --family=XYZ --store=666
...
```

Execute everything (will take hours/days)
```bash
python app/lstm_model.py
```


## Authors
Pedro Mayorga, PhD 

## License
For open source projects, say how it is licensed.

## Project status
Completed but submitted
