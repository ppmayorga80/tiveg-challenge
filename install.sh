#!/bin/bash

if [[ "${BASH_SOURCE[0]}" == "$0" ]]; then
  echo "Execute this script as a source:"
  echo "Usage:"
  echo "    source install.sh"
  echo ""
  exit 1
fi

if [[ ! -d "venv" ]]; then
  python3.10 -m venv venv
fi

source venv/bin/activate
export PYTHONPATH=$PWD:$PYTHONPATH
export PIPENV_VERBOSITY=-1

pip install --upgrade pip
pip install -r requirements.txt

KAGGLE_DATA=store-sales-time-series-forecasting
kaggle competitions download -c "${KAGGLE_DATA}"
unzip -n "${KAGGLE_DATA}.zip" -d "${KAGGLE_DATA}/"
