"""Given the pandas dataframes, this program will be fixed them
by removing null values or filling data when needed (for example
empty dates or constant values).
After this fixing operations we expand the fixed data by mapping
several fields to a single pandas dataframe with all rows
and columns we need
"""
import os
import re
from datetime import date, timedelta
from typing import Tuple, List

import pandas as pd
from dateutil.parser import parse as parse_str_as_date
from pandarallel import pandarallel
from tqdm import tqdm

from app.env import DATA_DIR, FILES

pandarallel.initialize(progress_bar=True, verbose=0)

HOLIDAY_IMPACT_RE_LIST = [
    (re.compile(r"Terremoto"), -5),
    (re.compile(r"Viernes Santo"), 2),
    (re.compile(r"Navidad"), 2),
    (re.compile(r"Primer dia del ano"), 2),
    (re.compile(r"Mundial.*Ecuador"), 3),
    (re.compile(r"Mundial"), 2),
    (re.compile(r"Black Friday"), 2),
    (re.compile(r"Carnaval"), 2),
    (re.compile(r"Cyber Monday"), 2),
    (re.compile(r"Dia de Difuntos"), 2),
    (re.compile(r"Dia de la Madre"), 2),
    (re.compile(r"Dia del Trabajo"), 2),
]


def closest_weekday_fn(dt: date) -> date:
    """Given a date, this function returns the
    first date within (dt,dt-1,dt-2,dt-3,...)
    that is a weekday
    """
    x = dt
    wd = dt.weekday()
    if wd >= 5:  # sat or sun (wd-4) is the delta for previous friday=4
        x = dt - timedelta(days=wd - 4)

    return x


def payday_apply_fn(x: pd.Series) -> bool:
    """Given a row x of a dataframe,
    this function returns True is x is day 15th or last day
    of the month (or closest weekday if needed).
    """
    d = parse_str_as_date(x['date']).date()

    # day 15th of current month/year
    p0 = date(d.year, d.month, 15)
    # last day of the month is 1 day before the 1st day of the next month
    # special case when month==12 -> next month=1 and year becomes next year
    p1 = date(d.year + (d.month % 12 == 0), d.month % 12 + 1, 1) - timedelta(days=1)

    # adjust to the closest weekday
    pd0 = closest_weekday_fn(p0)
    pd1 = closest_weekday_fn(p1)

    if (d == pd0) or (d == pd1):
        return True
    return False


def impact_apply_fn(x_holiday: pd.Series, re_list: List[Tuple[re.Pattern, int]]) -> int:
    description = x_holiday["description"]

    for rk, ik in re_list:
        if rk.findall(description):
            return ik
    return 1


def holiday_apply_fn(row: pd.Series, holidays: pd.DataFrame) -> Tuple[str, int]:
    """Given a dataframe row x, and a holidays dataframe
    this function returns a list of tuples with:
        0. holiday_type   in (Holiday, Transfer, Additional, Bridge, Work Day, Event)
        1. holiday_locale in (Local, Regional, National)
        2. impact         positive or negative integer depends on the impact of the holiday
    """

    row_date = row["date"]
    row_city = row["city"]
    row_state = row["state"]
    valid_holidays = holidays[(holidays['date'] == row_date) & (holidays['transferred'] == False)]
    list_of_valid_holidays = list(zip(valid_holidays["type"].to_list(),
                                      valid_holidays["locale"].to_list(),
                                      valid_holidays["locale_name"].to_list(),
                                      valid_holidays["impact"].to_list()))
    list_of_valid_holidays = [
        (fk[0], fk[1], fk[3])
        for fk in list_of_valid_holidays
        if (fk[1] == "National") or (fk[1] == "Local" and fk[2] == row_city) or (
                fk[1] == "Regional" and fk[2] == row_state)
    ]
    # sort by importance
    list_of_valid_holidays = sorted(list_of_valid_holidays, key=lambda x: -x[2])
    if list_of_valid_holidays:
        holiday_key = list_of_valid_holidays[0][0]
        holiday_val = list_of_valid_holidays[0][2]
        return holiday_key, holiday_val
    return "", 0


def fix_data(x: pd.DataFrame, stores_df: pd.DataFrame, oil_df: pd.DataFrame, holidays_df: pd.DataFrame) -> pd.DataFrame:
    """Given x and other dataframes, this functions fix x as follows:
    1. apply JOIN operator between (x,stores) -> df  : to have store information
    2. apply JOIN operator between (df,oil) -> df    : to have oil information
       fix oil column interpolating data
    3. add payday column to df
    4. add holiday column to df
    """

    # 0. fix holidays_events.csv <-> holidays dataframe
    holidays_df["impact"] = holidays_df.parallel_apply(
        impact_apply_fn,
        re_list=HOLIDAY_IMPACT_RE_LIST,
        axis=1
    )

    # 1. apply relational operations
    # 1.1 apply JOIN operation for training and store dataframes
    df = pd.merge(x, stores_df, how="left", left_on=["store_nbr"], right_on=["store_nbr"])

    # 2. apply join operation for df and oil dataframes
    df = pd.merge(df, oil_df, how="left", left_on=["date"], right_on=["date"])
    # 2.1 fix missing dcoilwtico column by using the nearest neighborhood algorithm
    df["dcoilwtico"] = df["dcoilwtico"].interpolate("nearest", limit_direction='both')
    df["dcoilwtico"] = df["dcoilwtico"].fillna(0.0)

    # 3. add payday column to df
    print("\nAdd payday column:")
    df["payday"] = df.parallel_apply(payday_apply_fn, axis=1)

    # 4. add holiday column to df
    print("\nAdd holiday column:")
    holiday_tuple = df.parallel_apply(holiday_apply_fn, holidays=holidays_df, axis=1)
    df[["holiday_name", "holiday_val"]] = pd.DataFrame(holiday_tuple.tolist(), index=df.index)

    # remove city and state columns
    df = df.drop(["city", "state"], axis=1)

    # finally dummy-fy columns

    return df


def main_fix_data(train_output_filename: str, test_output_filename: str, force: bool = False):
    if force or not os.path.exists(train_output_filename) or not os.path.exists(test_output_filename):
        # load data
        list_of_paths = [os.path.join(DATA_DIR, f"{f}.csv") for f in FILES]
        data = {
            f: pd.read_csv(p)
            for f, p in tqdm(zip(FILES, list_of_paths), total=len(FILES), desc="Loading CSV...")
        }

        # fixing train data
        print("\nFixing train data:")
        train_fixed = fix_data(x=data["train"],
                               stores_df=data["stores"],
                               oil_df=data["oil"],
                               holidays_df=data["holidays_events"])
        # fixing test data
        print("Fixing test data:")
        test_fixed = fix_data(x=data["test"],
                              stores_df=data["stores"],
                              oil_df=data["oil"],
                              holidays_df=data["holidays_events"])

        # save fixed data
        train_fixed.to_csv(train_output_filename, index=False)
        test_fixed.to_csv(test_output_filename, index=False)


if __name__ == '__main__':
    train_filename = os.path.join(DATA_DIR, "train_fixed.csv")
    test_filename = os.path.join(DATA_DIR, "test_fixed.csv")
    main_fix_data(train_filename, test_filename, force=True)
