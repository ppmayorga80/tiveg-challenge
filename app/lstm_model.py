#!/usr/bin/env python
"""LSTM Model will run Tiveg Challenge with given parameters

Usage:
    lstm_model [--verbose=LEVEL]
    lstm_model --debug [--plot] [--verbose=LEVEL] <FAMILY> <STORE>
    lstm_model --show-families
    lstm_model --show-stores

Options:
    <FAMILY>            a valid family to debug (see --show-families)
    <STORE>             a valid store to debug (see --show-stores)
    --debug             enter to debug mode for a given family and store
    --plot              in debug mode, show the predictions plot
    --verbose=LEVEL     show verbose level [default: 0]
    --show-families     will print a list of valid families
    --show-stores       will print a list of valid stores_nbr
"""

import multiprocessing
import os
from typing import List, Optional, Dict

import numpy as np
import pandas as pd
from docopt import docopt
from keras.callbacks import Callback
from keras.layers import Dense, LSTM
from keras.models import Sequential
from keras.models import load_model
from matplotlib import pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from tqdm import tqdm

from app.env import DATA_DIR
from app.fix_data import main_fix_data


def xmp_training_fn(arg):
    p = arg
    predictor = ModelPrediction(df=p["df"],
                                x_columns=p["x_columns"],
                                y_column=p["y_column"],
                                memory_window=p["memory_window"],
                                model_name=p["model_name"])
    predictor.load_or_create(force=False)

    g_debug = p["GLOBAL"].get("DEBUG", False)
    g_debug_family = p["GLOBAL"].get("DEBUG_FAMILY", "")
    g_debug_store = p["GLOBAL"].get("DEBUG_STORE", 0)
    g_show_plot = p["GLOBAL"].get("DEBUG_SHOW_PLOT", False)
    g_verbose = p["GLOBAL"].get("VERBOSE", 0)

    execution_flag = (g_debug and (p["family"] == g_debug_family) and (
            p["store"] == g_debug_store)) or (not g_debug)
    if execution_flag:
        predictor.train_and_predict(verbose=g_verbose)
        yp = predictor.predict(p["test_df"], verbose=g_verbose)
    else:
        yp = np.zeros((p["test_df"].shape[0],)) * np.nan

    predictor.save_model()

    if execution_flag:
        # example plot for one prediction
        loss = predictor.history['loss'][-1]
        val_loss = predictor.history['val_loss'][-1]

        df = predictor.df
        df0 = df[(df["family"] == p["family"]) & (df["store_nbr"] == p["store"])]
        t0 = np.arange(df0.shape[0])
        t0 = -t0[::-1]
        t1 = np.arange(1, 1 + yp.shape[0])
        if g_show_plot:
            plt.plot(t0, df0[p["y_column"]], "-k", label="history-sales")
            plt.plot(t0, df0["yp"], "--r", label="history-prediction")
            plt.plot(t1, yp, "-+b", label="future-prediction")
            plt.legend()
            plt.title(f"Family:{p['family']} & Store_nbr:{p['store']}\nLoss={loss:.2E} & ValLoss={val_loss:.2E}")
            plt.grid(True)
            plt.show()

    return p["family"], p["store"], yp


class StopAtLossValue(Callback):
    def __init__(self,
                 ref_dict: dict,
                 monitor='loss',
                 loss_success=1.0e-6,
                 test_loss=0.02,
                 test_epoch=100):
        super(Callback, self).__init__()
        self.ref_dict = ref_dict
        self.monitor = monitor
        self.loss_success = loss_success
        self.test_loss = test_loss
        self.test_epoch = test_epoch

    def on_epoch_end(self, epoch, logs={}):
        current = logs.get(self.monitor)

        self.ref_dict["error"] = False
        self.ref_dict["success"] = False
        if (epoch == self.test_epoch) and (current > self.test_loss):
            print(f"ERROR: at epoch {epoch} Model descent to slow")
            self.ref_dict["error"] = True
            self.model.stop_training = True

        if current <= self.loss_success:
            self.ref_dict["success"] = True
            self.model.stop_training = True


class ModelPrediction:
    def __init__(self, df, x_columns: List[str], y_column: str, memory_window: int = 20, model_name: str = ""):
        self.df = df
        self.x_columns = x_columns
        self.y_column = y_column
        self.model_name = model_name
        self.memory_window = memory_window
        self.x_scaler = MinMaxScaler()
        self.y_scaler = MinMaxScaler()

        self.n_cols = len(x_columns) + 1
        self.n_rows = df.shape[0]
        self.model: Optional[Sequential] = None
        self.history: Optional[Dict] = None
        self.yp: np.array = None

        self.delete_model_because_bad_training = False

    def save_model(self):
        if self.model and not self.delete_model_because_bad_training:
            self.model.save(self.model_name)
        if self.delete_model_because_bad_training:
            if os.path.exists(self.model_name):
                os.unlink(self.model_name)

    def load_or_create(self, force: bool = False) -> Sequential:
        if not force and os.path.exists(self.model_name):
            model = load_model(self.model_name)
            model.compile(optimizer="adam", loss="mean_squared_logarithmic_error")
        else:
            s1 = (self.memory_window, len(self.x_columns) + 1)
            n1 = int(1.0 * self.memory_window)
            n2 = int(0.5 * self.memory_window)
            n3 = 4
            n4 = 1
            model = Sequential()
            model.add(LSTM(n1, return_sequences=True, input_shape=s1))
            model.add(LSTM(n2, activation="relu", return_sequences=False))
            model.add(Dense(n3))
            model.add(Dense(n4, activation="relu"))
            model.compile(optimizer="adam", loss="mean_squared_logarithmic_error")

        self.model = model
        return model

    def train_and_predict(self, n_val: int = 20, verbose: int = 0):
        # 1. scale the matrix
        full_column_list = self.x_columns + [self.y_column]
        a = self.df[full_column_list].values
        ax = self.x_scaler.fit_transform(a)
        ay = self.y_scaler.fit_transform(a[:, -1:])

        # 2. envelope the matrix
        x = []
        y = []
        for i in range(self.memory_window, self.n_rows):
            x.append(ax[i - self.memory_window:i, :])
            y.append(ay[i, -1:])

        x = np.array(x)
        y = np.array(y)

        x_tra, y_tra = x[:-n_val, :], y[:-n_val, :]
        x_val, y_val = x[-n_val:, :], y[-n_val:, :]

        # build the LSTM
        batch_size = 100
        epochs = 1000
        # in case of not working well, delete the model and try again in next execution
        self.delete_model_because_bad_training = False
        ref_dict = {"success": None}
        early_error_detector = StopAtLossValue(ref_dict=ref_dict)
        self.model.fit(x_tra, y_tra,
                       batch_size=batch_size,
                       epochs=epochs,
                       shuffle=False,
                       validation_data=(x_val, y_val),
                       callbacks=[early_error_detector],
                       verbose=verbose)
        self.history = self.model.history.history
        # early stop detection
        if ref_dict["error"]:
            self.delete_model_because_bad_training = True

        # detail the training (final step)
        self.model.fit(x, y, batch_size=batch_size, epochs=100, verbose=verbose)

        # past predictions: fill the beginning with empty values (because the memory window)
        past_yp01 = self.model.predict(x, verbose=verbose)
        past_yp = self.y_scaler.inverse_transform(past_yp01)
        nn = np.zeros((self.memory_window, 1)) * np.nan
        nn_past_yp = np.concatenate((nn, past_yp), axis=0)
        self.df["yp"] = nn_past_yp

    def predict(self, df: pd.DataFrame, verbose: int = 0) -> np.array:
        prev_df = self.df.tail(self.memory_window)
        df["sales"] = np.nan
        dfx = pd.concat((prev_df, df), axis=0)

        full_column_list = self.x_columns + [self.y_column]
        a = dfx[full_column_list].values
        ax = self.x_scaler.transform(a)

        # 2. envelope the matrix
        yp01 = []
        for k in range(self.memory_window, dfx.shape[0]):
            xk = ax[k - self.memory_window:k, :]
            xk = xk.reshape(1, xk.shape[0], xk.shape[1])
            yk = self.model.predict(xk, verbose=verbose)
            ax[k, -1] = yk[0, 0]
            yp01.append(yk[0, 0])
        yp01 = np.array(yp01).reshape(-1, 1)
        yp = self.y_scaler.inverse_transform(yp01).reshape((-1,))

        return yp


class TivegSalePrediction:
    model_name_format = os.path.join(os.path.dirname(os.path.dirname(__file__)),
                                     "results",
                                     "model-family={family}_store={store}.h5")

    def __init__(self, x_columns: List[str], y_column: str, memory_window: int = 20, history_window: int = 240,
                 gdict: Optional[Dict] = None):
        self.train_filename = os.path.join(DATA_DIR, "train_fixed.csv")
        self.test_filename = os.path.join(DATA_DIR, "test_fixed.csv")
        self.original_test_filename = os.path.join(DATA_DIR, "test.csv")
        self.output_test_filename = os.path.join(DATA_DIR, "test-with-results.csv")

        self.x_columns = x_columns
        self.y_column = y_column
        self.memory_window = memory_window
        self.history_window = history_window
        self.gdict = gdict if gdict else {}

        main_fix_data(self.train_filename, self.test_filename, force=False)

        self.models = {}

    @classmethod
    def unique_values_fn_(cls, dfs: List[pd.DataFrame], on_column: str) -> List[str]:
        a = set()
        for df in dfs:
            ak = set(df[on_column])
            a = a.union(ak)

        b = sorted(list(a))
        return b

    def run_all(self):
        # at this point we are certain that "train_fixed.csv" and "test_fixed.csv" exists
        # fill na -> holiday_name, because holiday name is not important (was used to compute holiday_val)
        print("Reading CSV files ...")
        train_df = pd.read_csv(self.train_filename, low_memory=False)
        test_df = pd.read_csv(self.test_filename)

        # get the full list of families and stores
        family_list = self.unique_values_fn_([train_df, test_df], on_column="family")
        store_list = self.unique_values_fn_([train_df, test_df], on_column="store_nbr")

        # precompute all parameters
        param_list = []
        k = 0
        for family in tqdm(family_list, total=len(family_list), desc="Precompute parameters...", position=0,
                           leave=True):
            family_train_df = train_df[train_df["family"] == family]
            family_test_df = test_df[test_df["family"] == family]
            for store in tqdm(store_list, total=len(store_list), desc="Partial parameters", position=1, leave=False):
                store_family_train_df = family_train_df[family_train_df["store_nbr"] == store]
                store_family_test_df = family_test_df[family_test_df["store_nbr"] == store]

                family_norm = family.replace(" ", "-").replace("/", ".")
                param_list.append({
                    "k": k,
                    "family": family,
                    "store": store,
                    "df": store_family_train_df.tail(self.history_window),
                    "test_df": store_family_test_df,
                    "model_name": self.model_name_format.format(family=family_norm, store=store),
                    "x_columns": self.x_columns,
                    "y_column": self.y_column,
                    "memory_window": self.memory_window,
                    "GLOBAL": self.gdict,
                })
                k += 1

        with multiprocessing.Pool(8) as pool:
            responses = list(
                tqdm(
                    pool.imap_unordered(
                        xmp_training_fn,
                        param_list,
                        chunksize=8
                    ),
                    total=len(param_list),
                    desc="Training..."
                )
            )

        df = pd.read_csv(self.original_test_filename)
        df["sales"] = np.nan
        for family, store, yp in responses:
            rows = (df["family"] == family) & (df["store_nbr"] == store)
            df.loc[rows, "sales"] = yp

        df.to_csv(self.output_test_filename, index=False)


def main(**kwargs):
    args = docopt(__doc__, **kwargs)
    gdict = {"VERBOSE": int(args["--verbose"])}
    if args["--debug"]:
        gdict = {
            **{
                "DEBUG": True,
                "DEBUG_FAMILY": args["<FAMILY>"],
                "DEBUG_STORE": int(args["<STORE>"]),
                "DEBUG_SHOW_PLOT": args["--plot"],
            },
            **gdict
        }

    x_columns = ["onpromotion", "cluster", "dcoilwtico", "payday", "holiday_val"]
    y_column = "sales"

    model = TivegSalePrediction(x_columns=x_columns, y_column=y_column, memory_window=30, gdict=gdict)

    if args["--show-families"] or args["--show-stores"]:
        train_filename = os.path.join(DATA_DIR, "train.csv")
        test_filename = os.path.join(DATA_DIR, "test.csv")
        df0 = pd.read_csv(train_filename, low_memory=True)
        df1 = pd.read_csv(test_filename)
        column = "family" if args["--show-families"] else "store_nbr"
        values = TivegSalePrediction.unique_values_fn_(dfs=[df0, df1], on_column=column)
        print("\n".join([str(x) for x in values]))
    else:
        model.run_all()


if __name__ == '__main__':
    main()
