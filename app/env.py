import os

RELATIVE_DATA_DIR = "store-sales-time-series-forecasting"
DATA_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), RELATIVE_DATA_DIR)

FILES = [
    "holidays_events",
    "oil",
    "sample_submission",
    "stores",
    "test",
    "train",
    "transactions",
]
